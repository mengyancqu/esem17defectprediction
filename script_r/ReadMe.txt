
=====================================================
= Environment Setup
=====================================================
1.) Download and install R
2.) install the following packages: RWeka, car, usdm, effsize, ScottKnott by the following commands in R
	install.packages("RWeka")
	install.packages("car")
	install.packages("usdm")
	install.packages("effsize")
	install.packages("ScottKnott")


=====================================================
= Main script
=====================================================
- exeMain.r
[Overview]
It is the script of the experiment for the within-project and cross-project prediction.

[HowToRun]
In Terminal
> cd HOME/script_r
> R
> source("exeMain.r")

[Output]
../output/cross-validation/... are the results for within-project cross-validation
../output/cross-project/... are the results for cross-project prediction


- ReportResults.r
[Overview]
It is the script for presentations (figures and tables)

[HowToRun]
In Terminal
> cd HOME/script_r
> R
> source("ReportResult.r")
> q()

[Output]
all tables and figures will stored in the "results" folder

=====================================================
= Utility
=====================================================
- utils.r
It is the utility script for computing prediction performance and the number of files needed to be inspected.


