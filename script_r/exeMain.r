
### following RWeka packages can be downloaded in site: http://sourceforge.net/projects/weka/files/weka-packages/
RWeka::WPM("install-package", "script_r/packages/linearForwardSelection1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/ridor1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/EMImputation1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/citationKNN1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/isotonicRegression1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/paceRegression1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/leastMedSquared1.0.1.zip")
RWeka::WPM("install-package", "script_r/packages/RBFNetwork1.0.8.zip")
RWeka::WPM("install-package", "script_r/packages/conjunctiveRule1.0.4.zip")
RWeka::WPM("install-package", "script_r/packages/rotationForest1.0.2.zip")

coreExperiment <- function(fit, est, sampling=TRUE, seed=0) {
    ###### evaluation criterias ######
    Popt <- ACC <- NULL

    Ratio <- NULL
    
    fit.old <- fit
    est.old <- est

	### sampling
    if (sampling) {
        fit <- doSampling(fit, "bug", seed=seed)
    }

	### calc churn
    churn.fit.old <- decChurn(fit.old); churn.fit <- decChurn(fit)
    churn.est.old <- decChurn(est.old); churn.est <- decChurn(est)

    fbug.old <- fit.old$bug; fbug <- fit$bug
    ebug.old <- est.old$bug; ebug <- est$bug

    # log transformation for each measure except fix
    idx <- charmatch(c("wmc","dit","noc","cbo","rfc","lcom","ca","ce","npm","lcom3","loc","dam","moa","mfa","cam","ic","cbm","amc","max_cc","avg_cc"), colnames(fit))
    fit[,idx] <- fit[,idx] + 1
    idx <- charmatch(c("bug"), colnames(fit))
    fit[,-c(idx)] <- apply(fit[,-c(idx)], 2, log)

    ###### remove number of code from the prediction model
    idx <- charmatch(c("loc"), colnames(fit))
    fit <- fit[,-c(idx)]

    ### remove those variables with 0 variance
    va <- apply(fit, 2, var)
    fit <- fit[,((!is.na(va))&(va!=0))] ### fit <- fit[,!(va==0)]

    err <- try(vifstep.names <- as.character(usdm::vifstep(fit[, setdiff(colnames(fit), "bug")], th=10)@results$Variables), silent=TRUE)
    if (class(err)!="try-error") {
        fit <- fit[, c(vifstep.names, "bug")]            
    }

    ### VIF analysis
    idx <- charmatch(c("bug"), colnames(fit))
    x <- fit[,-c(idx)]
    err <- try(x <- remVarsByVIF(x), silent=TRUE)
    if (class(err)!="try-error") {
        fit <- cbind(x, fit["bug"])
    }

    ### log transformation for est
    idx <- charmatch(c("wmc","dit","noc","cbo","rfc","lcom","ca","ce","npm","lcom3","loc","dam","moa","mfa","cam","ic","cbm","amc","max_cc","avg_cc"), colnames(est))
    est[,idx] <- est[,idx] + 1
    idx <- charmatch(c("bug"), colnames(est))
    est[,-c(idx)] <- apply(est[,-c(idx)], 2, log)

    ### evaluate the prediction performance when considering effort (effort=churn)
    fit$bugdensity <- fit$bug/(churn.fit+1)
    est$bugdensity <- est$bug/(churn.est+1)

    fit.old$bugdensity <- fit.old$bug/(churn.fit.old+1)
    est.old$bugdensity <- est.old$bug/(churn.est.old+1)

    getWekaClassifier <- function(name) {
        WC <- NULL

        if (name %in% c("LinearRegression", "Logistic", "SMO", "IBk", "LBR", "AdaBoostM1", "Bagging", "LogitBoost", "MultiBoostAB", "Stacking", "CostSensitiveClassifier", "JRip", "M5Rules", "OneR", "PART", "J48", "LMT", "M5P", "DecisionStump", "SimpleKMeans")) {
            WC <- get(name, asNamespace("RWeka"))
        } else {
            if (name=="NaiveBayes") {
                WC <- RWeka::make_Weka_classifier("weka/classifiers/bayes/NaiveBayes")
            } else if (name %in% c("GaussianProcesses", "SimpleLogistic", "SimpleLinearRegression", "IsotonicRegression", "LeastMedSq", "MultilayerPerceptron", "PaceRegression", "PLSClassifier", "RBFNetwork", "SMOreg")) {
                WC <- RWeka::make_Weka_classifier(paste("weka/classifiers/functions/", name, sep=""))
            } else if (name %in% c("KStar", "LWL")) {
                WC <- RWeka::make_Weka_classifier(paste("weka/classifiers/lazy/", name, sep=""))
            } else if (name %in% c("Bagging", "AdaBoostM1", "RotationForest", "RandomSubSpace", "AdditiveRegression", "AttributeSelectedClassifier", "CVParameterSelection", "Grading", "GridSearch", "MultiScheme", "RegressionByDiscretization", "StackingC", "Vote")) {
                WC <- RWeka::make_Weka_classifier(paste("weka/classifiers/meta/", name, sep=""))
            } else if (name %in% c("ConjunctiveRule", "DecisionTable", "ZeroR", "Ridor")) {
                WC <- RWeka::make_Weka_classifier(paste("weka/classifiers/rules/", name, sep=""))
            } else if (name %in% c("REPTree", "UserClassifier", "RandomForest")) {
                WC <- RWeka::make_Weka_classifier(paste("weka/classifiers/trees/", name, sep=""))
            }
        }
        return(WC)
    }

    subFunc <- function(data.train, data.valid, method=NULL, meta=NULL, base=NULL, yname.numeric=NULL, yname.nominal=NULL, xnames=NULL, xname=NULL) {
        if (is.null(method)) {
            if (!is.null(meta)) { method <- paste(meta, base, sep="+") } else { method <- base }
        } else {
            base <- method
        }

        pred.train <- pred.valid <- NULL

        if (grepl("UP", method)) {
            pred.train <- 10000/(data.train[, xname]+1)
            pred.valid <- 10000/(data.valid[, xname]+1)
        } else if (method=="EALR") {
            formula <- as.formula(paste(yname.numeric, paste(xnames, collapse="+"), sep="~"))
            model <- glm(formula=formula, data=data.train, family=gaussian)
            try(model <- step(model, k=log(nrow(data.train)), trace=FALSE), silent=TRUE)

            if (length(attr(model$terms, "term.labels"))>=2) {
                err <- try(model.vif <- max(car::vif(model)), silent=TRUE)
                if (class(err)!="try-error") {
                    if (!is.na(model.vif)) {
                        if (model.vif > 10) { cat("VIF of EALR model is larger than 10.\n") }
                    }
                }
            }

            pred.train <- predict(model, new=data.train)
            pred.valid <- predict(model, new=data.valid)
            step.xnames <- attr(model$terms, "term.labels")
            for (aname in step.xnames) {
                data.train[, aname] <- (data.train[, aname]-min(data.train[, aname]))/(max(data.train[, aname])-min(data.train[, aname]))
                data.valid[, aname] <- (data.valid[, aname]-min(data.valid[, aname]))/(max(data.valid[, aname])-min(data.valid[, aname]))
            }
        } else if (!is.null(meta)) {
            data.train[, yname.nominal] <- as.factor(data.train[, yname.nominal])
            formula <- as.formula(paste(yname.nominal, paste(xnames, collapse="+"), sep="~"))
            ########################################
            ### Ensemble methods
            ########################################
            WC <- getWekaClassifier(meta)
            BC <- getWekaClassifier(base)
            model <- WC(formula=formula, data=data.train, control=RWeka::Weka_control(W=BC))
            
            if (is.null(yname.numeric)) {
                pred.train <- predict(model, new=data.train, type="probability")[, "1"]
                pred.valid <- predict(model, new=data.valid, type="probability")[, "1"]
            } else {
                pred.train <- predict(model, new=data.train)
                pred.valid <- predict(model, new=data.valid)
            }
        } else {
            data.train[, yname.nominal] <- as.factor(data.train[, yname.nominal])
            formula <- as.formula(paste(yname.nominal, paste(xnames, collapse="+"), sep="~"))

            WC <- getWekaClassifier(base)
            model <- NULL
            if (method=="IBk") {
                model <- WC(formula=formula, data=data.train, control=RWeka::Weka_control(K=8))
            } else {
                model <- WC(formula=formula, data=data.train)
            }

            if (is.null(yname.numeric)) {
                pred.train <- predict(model, new=data.train, type="probability")[, "1"]
                pred.valid <- predict(model, new=data.valid, type="probability")[, "1"]
            } else {
                pred.train <- predict(model, new=data.train)
                pred.valid <- predict(model, new=data.valid)
            }
        }

        pred.train <- as.vector(pred.train)
        pred.valid <- as.vector(pred.valid)
        train.dt <- valid.dt <- NULL
        if (grepl("UP", method)) {
            train.dt <- data.frame(NUM=fbug.old, REL=fbug.old, LOC=churn.fit.old, PRE=pred.train)
            valid.dt <- data.frame(NUM=ebug.old, REL=ebug.old, LOC=churn.est.old, PRE=pred.valid)
        } else {
            train.dt <- data.frame(NUM=fbug, REL=fbug, LOC=churn.fit, PRE=pred.train)
            valid.dt <- data.frame(NUM=ebug, REL=ebug, LOC=churn.est, PRE=pred.valid)
        }

        sorted           <- FALSE
        worstcase        <- TRUE  ### compute the worst performance for unsupervised models
        bestcase         <- FALSE
        LOCUP            <- FALSE
        allpercentcutoff <- TRUE
        sub.Popt <- sub.ACC <- NULL
        if (grepl("UP", method)) {
            sub.Popt <- ComputePopt(sorted=sorted, data=valid.dt, worstcase=worstcase, bestcase=bestcase, LOCUP=LOCUP)
            sub.ACC <- ComputeACC(sorted=sorted, data=valid.dt, worstcase=worstcase, bestcase=bestcase, LOCUP=LOCUP)
            sub.Ratio <- ComputeRatio(sorted=sorted, data=valid.dt, worstcase=worstcase, bestcase=bestcase, LOCUP=LOCUP , method = method)
        } else {
            sub.Popt <- ComputePopt(sorted=sorted, data=valid.dt)
            sub.ACC <- ComputeACC(sorted=sorted, data=valid.dt)
            sub.Ratio <- ComputeRatio(sorted=sorted, data=valid.dt, method = method)
        }

        names(sub.Ratio) <- paste(method,"Ratio", sep=".")
        Ratio <<- c(Ratio, sub.Ratio)
        names(sub.Popt) <- paste(method, "Popt", sep=".")
        Popt <<- c(Popt, sub.Popt)
        names(sub.ACC) <- paste(method, "ACC", sep=".")
        ACC <<- c(ACC, sub.ACC)
    }
    
    
    yname.numeric <- "bugdensity"; yname.nominal <- "bug"
    xnames <- setdiff(colnames(fit), c("bug", "bugdensity"))
    subFunc(method="EALR", data.train=fit, data.valid=est, yname.numeric=yname.numeric, xnames=xnames)
    for (method in c("NaiveBayes", "SimpleLogistic", "LMT", "RBFNetwork", "JRip", "Logistic", "RandomForest", "SMO", "J48", "Ridor", "IBk")) { ### 
        subFunc(data.train=fit, data.valid=est, method=method, yname.numeric=NULL, yname.nominal=yname.nominal, xnames=xnames)
    }

    for (meta in c("Bagging", "RotationForest", "AdaBoostM1", "RandomSubSpace")) { ### ensemble methods
        bases <- c("SimpleLogistic", "LMT", "NaiveBayes", "SMO", "J48")
        for (base in bases) {
            subFunc(data.train=fit, method=NULL, meta=meta, base=base, data.valid=est, yname.numeric=NULL, yname.nominal=yname.nominal, xnames)
        }
    }

    cnames <- c("wmc","dit","noc","cbo","rfc","lcom","ca","ce","npm","lcom3","dam","moa","mfa","cam","ic","cbm","amc","max_cc","avg_cc")
    labels <- c("WMC","DIT","NOC","CBO","RFC","LCOM","CA","CE","NPM","LCOM3","DAM","MOA","MFA","CAM","IC","CBM","AMC","MAX_CC","AVG_CC")

    for (i in seq(cnames)) {
        xname <- cnames[i]; label <- labels[i]
        methods <- paste(label, "UP", sep=".")

        for (method in methods) {
            subFunc(method=method, data.train=fit.old, data.valid=est.old, xname=xname)
        }
    }

    #######################################################################
    ### -> Output
    return(list(Popt=Popt, ACC=ACC, Ratio=Ratio))
}

### 10 times 10-fold cross-validation
exeExperiment.crossvalidation <- function(project, sampling=TRUE, totalFolds=10, totalRuns=10) {
    Popt <- ACC <- NULL
    Ratio <- NULL
    all.name <- paste(project, ".csv", sep="")
   # file <- file.path("C:/Users/fyc/Desktop/jit", "input", all.name, fsep=.Platform$file.sep)
   file <- file.path("input", all.name, fsep=.Platform$file.sep)
    if (!file.exists(file)) { return(NULL) }
    all.data <- read.csv(file=file, header=TRUE, row.names=1)

    cat("(BEG)Cross-validation", "for", project, "\n")

    theList <- NULL
    index <- 1
    for (run in seq(totalRuns)) {
        sub <- divideToKsubsets(data=all.data, k=totalFolds, seed=run)
        for (fold in seq(totalFolds)) {
            fit.name <- paste(project, "_fit_", fold, sep="")
            est.name <- paste(project, "_est_", fold, sep="")
            fit <- est <- NULL
            for (j in seq(totalFolds)) {
                if (j != fold) {
                    fit <- rbind(fit, sub[[j]])
                }
            }
            est <- sub[[fold]]
            tmp.res <- coreExperiment(fit=fit, est=est, sampling=sampling, seed=fold)
            theList[[index]] <- tmp.res
            index <- index + 1
        }
    }

    cat("(END)Cross-validation", "for", project, "\n")

    Popt.lst <- sapply(theList, '[', "Popt")
    ACC.lst <- sapply(theList, '[', "ACC")

    Ratio.lst <- sapply(theList, '[', "Ratio")
    
    for (i in seq(totalRuns*totalFolds)) {
        Popt <- rbind(Popt, Popt.lst[[i]])
        ACC <- rbind(ACC, ACC.lst[[i]])

        Ratio <- rbind(Ratio, Ratio.lst[[i]])
    }

    return(list(Popt=Popt, ACC=ACC, Ratio=Ratio))
}

### cross project validation
exeExperiment.crossproject <- function(project.fit, project.est, sampling=TRUE) {
    Popt <- ACC <- NULL
    Ratio <- NULL
    cat("(BEG)Across-project", project.fit, ":", project.est, "\n")

    fit.name <- paste(project.fit,".csv",sep="")
    fit.data <- read.csv(file.path("input", fit.name, fsep=.Platform$file.sep), header=TRUE, row.names=1)

    est.name <- paste(project.est,".csv",sep="")
    est.data <- read.csv(file.path("input", est.name, fsep=.Platform$file.sep), header=TRUE, row.names=1)

    tmp.res <- NULL
    tmp.res <- coreExperiment(fit=fit.data, est=est.data, sampling=sampling, seed=0)
    
    cat("(END)Across-project", project.fit, ":", project.est, "\n")

    Popt <- rbind(Popt, tmp.res$Popt)
    ACC <- rbind(ACC, tmp.res$ACC)
    Ratio <- rbind(Ratio, tmp.res$Ratio)

    return(list(Popt=Popt, ACC=ACC ,Ratio=Ratio))
}

printResult <- function(tmp, path, name) {
    out.fname <- paste(path, "out_", name, ".txt", sep="")
    dput(tmp, file=out.fname)

    out.fname <- paste(path, "out_", name, "_Popt.txt", sep="")
    dput(tmp$Popt, file=out.fname)

    out.fname <- paste(path, "out_", name, "_ACC.txt", sep="")
    dput(tmp$ACC, file=out.fname)

    out.fname <- paste(path, "out_", name, "_Ratio.txt", sep="")
    dput(tmp$Ratio, file=out.fname)
}

########################################
### output cross validation results
########################################
subFunc.cv <- function(project, sampling=TRUE) {
    path <- "output/cross-validation/"
    if (!file.exists(file.path(path))) { dir.create(path) }
    file <- paste("input/", project, ".csv", sep="")
    if (!file.exists(file)) { return(NULL) }
    file <- paste(path, "out_", project, "_ce.txt", sep="")
    if (file.exists(file)) { return(NULL) }
    tmp <- exeExperiment.crossvalidation(project=project, sampling=sampling)
    printResult(tmp, path, project)
}

########################################
### output cross-project performance
########################################
subFunc.cp <- function(arg, sampling=TRUE) {
    path <- "output/cross-project/"
    if (!file.exists(file.path(path))) { dir.create(path) }
    file1 <- paste("input/", arg[1], ".csv", sep="")
    file2 <- paste("input/", arg[2], ".csv", sep="")
    if (!(file.exists(file1))&&(file.exists(file2))) { return(NULL) }
    file <- paste(path, "out_", arg[1], "_cross_", arg[2], "_ce.txt", sep="")
    if (file.exists(file)) { return(NULL) }
    tmp <- exeExperiment.crossproject(project.fit=arg[1], project.est=arg[2], sampling=sampling)
    printResult(tmp, path, paste(arg[1], "_cross_", arg[2], sep=""))
}

source("script_r/utils.R")

projects <- c("ant-1.7", "camel-1.6","ivy-1.4","jedit-4.0","log4j-1.0","velocity-1.6","poi-2.0","tomcat-6.0","xalan-2.4","xerces-1.3")

#### cross-validation
for (project in projects) {
    subFunc.cv(project) 
}

#### across-project prediction
args <- NULL; index <- 1
for (i in seq(projects)) {
    for (j in seq(projects)) {
        if (i==j) { next }
        args[[index]] <- c(projects[i], projects[j])
        index <- index + 1
    }
}

for (arg in args) {
    subFunc.cp(arg)
}